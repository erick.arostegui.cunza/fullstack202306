﻿using DotNet_API.Classic.Application.Dto;

namespace DotNet_API.Classic.Application
{
    public interface IPersonService
    {
        List<PersonDto> GetPeople();
    }
}