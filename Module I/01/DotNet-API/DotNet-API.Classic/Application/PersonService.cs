﻿using DotNet_API.Classic.Application.Dto;
using DotNet_API.Classic.Infrastructure.Data;

namespace DotNet_API.Classic.Application
{
    public class PersonService : IPersonService
    {
        private List<Person> _people;

        public PersonService()
        {
            CreatePeople();
        }

        public List<PersonDto> GetPeople()
        {
            List<PersonDto> people = new List<PersonDto>();
            _people.ForEach(person =>
            {
                people.Add(new PersonDto { Name = person.Name, LastName = person.LastName, Age = person.Age });
            });

            return people;
        }


        private void CreatePeople()
        {
            _people = new List<Person>();
            _people.Add(new Person { Name = "Erick", LastName = "Arostegui", Age = 40 });
            _people.Add(new Person { Name = "Juan", LastName = "Perez", Age = 39 });
            _people.Add(new Person { Name = "Jose", LastName = "Lopez", Age = 25 });
            _people.Add(new Person { Name = "Pedro", LastName = "Picapiedra", Age = 21 });
            _people.Add(new Person { Name = "Luis", LastName = "Rodriguez", Age = 32 });
            _people.Add(new Person { Name = "Jorge", LastName = "Diaz", Age = 37 });
            _people.Add(new Person { Name = "María", LastName = "Sarmiento", Age = 19 });
        }

    }
}
