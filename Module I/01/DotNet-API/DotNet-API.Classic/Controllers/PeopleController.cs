﻿using DotNet_API.Classic.Application;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DotNet_API.Classic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly IPersonService _personService;
        public PeopleController(IPersonService personService)
        {
            _personService = personService;
        }
                
        [HttpGet]
        public IActionResult GetPeople()
        {
            return Ok(_personService.GetPeople());
        }
    }
}
