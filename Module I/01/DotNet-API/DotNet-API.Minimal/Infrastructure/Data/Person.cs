﻿namespace DotNet_API.Minimal.Infrastructure.Data
{
    public class Person
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
