﻿using DotNet_API.Minimal.Application.Dto;

namespace DotNet_API.Minimal.Application
{
    public interface IPersonService
    {
        List<PersonDto> GetPeople();
        PersonDto GetPersonByName(string name);
    }
}