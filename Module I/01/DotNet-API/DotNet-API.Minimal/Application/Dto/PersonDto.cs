﻿namespace DotNet_API.Minimal.Application.Dto
{
    public class PersonDto
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
